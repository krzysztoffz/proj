import React, {useState} from 'react';
import {Link} from 'react-router-dom';
const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop);

const Header2 = ({refPo, refL, refD, refPu, refG}) =>{
	return(
	<div>
		<ul>
    		<li onClick={()=>scrollToRef(refPo)}>Polana</li>
    		<li onClick={()=>scrollToRef(refL)}>Las</li>
    		<li onClick={()=>scrollToRef(refD)}>Dzngla</li>
    		<li onClick={()=>scrollToRef(refPu)}>Pustynia</li>
    		<li onClick={()=>scrollToRef(refG)}>Gory</li>
    	</ul>
    	</div>
	)
}
export default Header2;