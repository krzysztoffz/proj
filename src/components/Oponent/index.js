//połączenie user z context

import React, {useContext} from 'react';
import {CharStats} from '../../context/index.js';
import TableRow from '../TableRow';
import {useState} from 'react';


const Oponent = (props) =>{
	const {name, str, hp, speed, damage, lvl} = useContext(CharStats);
	const myStats = {name, str, hp, speed, damage, lvl};
	return(

	<div className="col-6">
		{props.alive && 
			<TableRow {...myStats}/>
		}
	</div>
	)
}

export default Oponent;