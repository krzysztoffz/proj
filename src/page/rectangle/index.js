import React, {Component, useState, useRef,useEffect, createContext} from 'react';
import styled from 'styled-components';
//import RectangleResult from './rectangleResult';
const MyRectangle = styled.div`
width: ${props => props.width+'px'};
height: ${props => props.height+'px'};
background-color:black;
display: flex;
text-align: center;
align-items: center;
justify-content:center;
transition: all 0.5s;
`; 

const Rectangle = (props) => {
	
const state = createContext({
	width:103,
	height:120,
	max:500,
	min:100
})

	const handleResize = () =>{
		const max = state.max;
		const min = state.min;
		let width = Math.floor(Math.random() * (max - min)) + min;
		let height = Math.floor(Math.random() * (max - min)) + min;
		props.state.setState({width,height})
	};

	const RectangleResult = (width, height) => {
		const ref = width*height;
		return ref;
	};

	let {width, height} = state;
	const ref = React.createRef();
	return(
		<div>
		<MyRectangle ref={ref} width={width} height={height}>
		<RectangleResult {...{ref}}/>
		</MyRectangle>
		<p>Szerokość: {width} px</p>
		<p>Wysokość: {height} px</p>
		<button onClick={()=>handleResize()}>Generuj nowy element </button>
		</div>
	);
};
export default Rectangle