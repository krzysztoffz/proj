import React, {Component, useState, useRef,useEffect, createContext} from 'react';
import syled from 'styled-components';
//import RectangleResult from './rectangleResult';
const MyRectangle = styled.div`
width: ${props => props.width+'px'};
height: ${props => props.height+'px'};
background-color:black;
display: flex;
text-align: center;
align-items: center;
justify-content:center;
transition: all 0.5s; 
`; 

class Rectangle extends Component {
	constructor(){
		super()
		this.state ={
			width:103,
			height:120,
			max:500,
			min:100}
	}


RectangleResult = (width, height) => {
	const ref = useRef(null)
	ref = width*height;
	return ref;
};
	
handleResize = () =>{
	const max = this.state.max;
	const min = this.state.min;
	let width = Math.floor(Math.random() * (max - min)) + min;
	let height = Math.floor(Math.random() * (max - min)) + min;
	this.setState({width, height})
}
render (){
	let {width, height} = this.state;
	const ref = React.createRef();
	return(
	<div className = 'rectangle_wrapper'>
	<MyRectangle ref = {ref} width={width} height={height}>
	<RectangleResult {...{ref}}/>
	</MyRectangle>
	<p>Szerokość: {width} px</p>
	<p>Wysokość: {height} px</p>
	<button onClick={()=>this.handleResize()}>Generuj nowy element </button>
	</div>
	)
}}
export default Rectangle