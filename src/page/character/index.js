import React , {useContext, useEffect, Profiler, useRef}from 'react';
import Header from '../../components/Header';
import Header2 from '../../components/Header/index2.js';
import User from '../../components/User';
import Oponent from '../../components/Oponent';
import Klasa from '../../components/Klasa';
import {UserStatsProvider, OponentStatsProvider, GameState, CharStats, OponentStatsProvider2} from '../../context';
import Form from '../../components/Form';
import MyText from '../../components/MyTest/index.js';
import Oponent2 from '../../components/Oponent2';
import ButtonChange from '../../components/Oponent2';

const Character = () => {
	const {name, str, hp, speed, addStr} = useContext(CharStats);
  const {isName, setName} = useContext(GameState);
useEffect(()=>{
    const getName = localStorage.getItem('name');
    if (getName.length > 0) {setName(true)}}, [isName])
	return(
	<div>
	User
	 {!isName ? (
         <div>
         <UserStatsProvider>
    	       <User />
    	     </UserStatsProvider>
	       </div>
        ) : (
          <Form />
        )}
	
	</div>
	);
};
export default Character;