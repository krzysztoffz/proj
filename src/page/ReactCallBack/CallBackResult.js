import React, {useState, useEffect, useCallback} from 'react'

const CallBackResult = ({elPos,reverse})=> {
	
const testFunction = () => {
		console.log(elPos)
	}
	
	useEffect(()=>{
		testFunction();
		console.log('test function changed - ta funkcja została zmieniona')
	},[testFunction, reverse])

const testFunction = useCallback(() => {
		console.log(elPos)
	}, [reverse]) 	
	
	return {
		<div>
		<p>top: {elPos.top} px</p>
		<p>left: {elPos.left} px</p>
		<button onClick ={()=> testFunction()}> check function</button>
		</div>
		)
	};
	export default CallBackResult;