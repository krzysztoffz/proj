import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Homepage from './page/homepage';
import Game from './page/game';
import Character from './page/character';
import SlowSuspense from './components/SlowSuspense';
import Spinner from './components/Loader';
import Header from './components/Header';
import Rectangle from './page/rectangle';

function App() {
  return (
    <Rectangle />
  );
}

export default App;
