import React, {useEffect, useState, Suspense} from 'react';
export const SlowSuspense = ({childern, fallback=null}) => {
	const [show, setShow] = useState(true);
useEffect(()=>{
	setTimeout (()=>setShow(false), 1000);
},[])

  return (
  <div>
  {show && fallback}
  <Suspense fallback={fallback}>
	  {childern}
	  </Suspense>
  </div>
  );
}

export default SlowSuspense;
