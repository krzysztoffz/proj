//USer
//pobieranie styli
import './style.css';
import {useContext, useEffect} from 'react';
import {CharStats} from '../../context/index.js';
import TableRow from '../TableRow';


const User = () =>{
	const {name, str, hp, speed, addName, addStr, damage, lvl} = useContext(CharStats);
	const myStats = {name, str, hp, speed, damage, lvl};

	useEffect(()=> {
		addName(localStorage.getItem('name')
)
	},[])

	return(
	<div className="col-6">
	<TableRow {...myStats}/>
	</div>
	)
}
export default User;


