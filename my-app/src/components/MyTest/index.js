import {useState} from 'react'
import MyMouse from './myMouse'
import MyTxt from './myTxt'

const MyText = props => {
	return(
	<div>
	<MyMouse render={mousePos => (
		<MyTxt mousePos={mousePos}/>)}/>
</div>
	)
}
export default MyText;