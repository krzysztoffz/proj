import React, {useContext, useState, useEffect } from 'react'; 
import {GameState} from '../../context';

const Form = ()  => { 
const {isName, setName} = useContext(GameState);
const [name, addName] = useState('');

const createName = (item) =>{
	return item;
}

const myName = createName('test');

useEffect(() => {
console.log(myName)
createName('Piotr')
}, [myName])


const handleSubmit = (e) => {
	e.preventDefault();
	setName(true)
	localStorage.setItem('name',name)
}
    return (
		<form onSubmit={e => handleSubmit(e)}>
		<label>name: </label>
		<input name='name' type='text' value={name} onchange={(e) =>addName(e.target.value)}/>
		<input name='send' type='submit'/>
		</form>
    )
}

export default Form;
