prompt
if (true) {
	alert('uzupełnij załącznik nr 1');
}


import React, {createContext, useState} from 'react';

export const CharStats = createContext({
	name: "dee",
	str: 0,
	hp: 0,
	speed:10,
	damage:0,
	lvl:1,
	addStr: item =>{},
	addName: item =>{}
})

export const GameState = createContext({
	isName: false,
	setName: item => {}
})


export const GameApp = ({children})  => {
	const [isName, setName] = useState(false);
	return(
	<GameState.Provider 
	value={{isName, setName}}>
	{children}
	</GameState.Provider>
	)
}

export const ValState = createContext({
	isVal: false,
	setVal: item => {}
})

export const ValApp = ({children})  => {
	const [isVal, setVal] = useState(false);
	return(
	<ValState.Provider 
	value={{isVal, setVal}}>
	{children}
	</ValState.Provider>
	)
}

export const UserStatsProvider = ({children})  => {
	const [name, addName]= useState('');
	const [str, addStr] = useState(10);
	const hp = 10;
	const speed = 2;
	const damage = speed * str;
	const lvl = 1;
	return(
	<CharStats.Provider 
	value={{name, str, hp, speed, addName, addStr, damage, lvl}}>
	{children}
	</CharStats.Provider>
	)
}

export const OponentStatsProvider = ({children})  => {
	const x = localStorage.getItem('dane');
	const name = 'opo';
	const str = x[0];
	const hp = x[2];
	const speed = x[4];
	const damage = speed * str;
	const lvl = x[6];	
	return(
	<CharStats.Provider
	value={{name, str, hp, speed, damage, lvl}}
	>
	{children}
	</CharStats.Provider>
	)
}



// export const OponentStatsProvider2 = ({children})  => {

// 	const name = 'opo';
// 	const [newstr] = useState('');
// 	const [newhp] = useState('');
// 	const [newspeed] = useState('');
// 	const damage = [newspeed] * [newstr];
// 	const [newlvl] = useState('');	
// 	return(
// 	<CharStats.Provider
// 	value={{name, newstr, newhp, newspeed, damage, newlvl}}
// 	>
// 	{children}
// 	</CharStats.Provider>
// 	)
// }




















