import React, {Component, useState, useRef,useEffect} from 'react';
import styled from 'styled-components';
import RectangleResult from './rectangleResult';
const MyRectangle = styled.div`
width: ${props => props.width+'px'};
height: ${props => props.height+'px'};
background-color:black;
display: flex;
text-align: center;
align-items: center;
justify-content:center;
transition: all 0.5s;
`; 

const Rectangle = (props) => {
	props.state ={
		width:103,
		height:120,
		max:500,
		min:100};
	
	const handleResize = () =>{
		const max = props.state.max;
		const min = props.state.min;
		let width = math.floor(math.random() * (max - min)) + min;
		let height = math.floor(math.random() * (max - min)) + min;
		props.setState({width, height})
	};

	let {width, height} = props.state;
	const ref = React.createRef();
	return(
		<div className = 'rectangle_wrapper'>
		<MyRectangle ref={ref} width={width} height={height}>
		<RectangleResult {...{ref}}/>
		<MyRectangle>
		<p>Szerokość: {width} px</p>
		<p>Wysokość: {height} px</p>
		<button onClick={()=>props.handleResize()}>Generuj nowy element </button>
		</div>
	);
};
export default Rectangle