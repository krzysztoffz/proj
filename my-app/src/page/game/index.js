import React , {useContext, useEffect, Profiler, useRef}from 'react';
import Header from '../../components/Header';
import Header2 from '../../components/Header/index2.js';
import User from '../../components/User';
import Oponent from '../../components/Oponent';
import Klasa from '../../components/Klasa';
import {UserStatsProvider, OponentStatsProvider, GameState, CharStats, OponentStatsProvider2} from '../../context';
import Form from '../../components/Form';
import MyText from '../../components/MyTest/index.js';
import Oponent2 from '../../components/Oponent2';
import ButtonChange from '../../components/Oponent2';

const Game = () => {
	const {name, str, hp, speed, addStr} = useContext(CharStats);
  const {isName, setName} = useContext(GameState);
	const refPo = useRef(null);
  const refL = useRef(null);
  const refD = useRef(null);
  const refPu = useRef(null);
  const refG = useRef(null);
const y = localStorage.getItem('dane');


	return(
	<div>
	
	<Header2 {...{refPo, refL, refD, refPu, refG}}/>
	{y ? (
              <div>
    	     <OponentStatsProvider>
    	       <Oponent alive/>
    	     </OponentStatsProvider>
             <Oponent2 alive/>
             </div>
             ) : (
             <div>
             <Oponent2 alive/>
             </div>
             )}
             <div className="col-12 destination" ref={refPo}>Polana</div>
         <div className="col-12 destination" ref={refL}>Las</div>
         <div className="col-12 destination" ref={refD}>Dzngla</div>
         <div className="col-12 destination" ref={refPu}>Pustynia</div>
         <div className="col-12 destination" ref={refG}>Gory</div>
	</div>
	);
};
export default Game;